#!/bin/sh
# File: Instalar
sh remove.sh
echo '
## Sistema de carga para el servidor local
##
## @author      Villalba Juan Manuel Pedro <https://github.com/juanma386>
# @Version:     0.0.1
# @Estado:      Alpha
# @Mail:		hexomedesarrollos@gmail.com
# @Comentario:	Ayudarte a vos amigo que estas del otro lado luchando para entender todo esto, se que es dificil aprender pero todo se logra, acepto donaciones :D
# @Destino:     Instalar Codeigniter Linux
# @Date:        05:41pm 30-04-2019
# @Advertencia  Usar con cautela y bajo propia responsabilidad
# @Licence:     GPLv2 Free Software Foundation <licensing@fsf.org>
'

FOLDERPROYECT=$(pwd)/code;
_hoy=$(date +"%m_%d_%Y_%r_%S")
_pwd=$(pwd)
_permisos=$(ls -la *)
bin=$FOLDERPROYECT/./bin

instalarbinarios(){
mkdir $FOLDERPROYECT && mkdir bin
cp . $FOLDERPROYECT/code/bin/install.php 
}

if [ ! -e "$bin" ];
then
echo "No existe $bin"

else
echo "$bin Si existe"

fi

instalared(){
composer create-project kenjis/codeigniter-composer-installer $FOLDERPROYECT
}
# Crear nube desplegable:
FLAG=$FOLDERPROYECT"/archivo-flag.log"
if [ ! -f $FLAG ]; then
    echo "Ahora pasamos a realizar la carga del sistema de nuevo"
    # Crear flag para recordar que se ejecuto
    instalared
	touch $FLAG
else
    echo "Al parecer existe un problema que escapa de esta instancia"
    exit 0
fi

permisosverificarserver(){
if [ ! -x "$bin/servicio.sh" ];
then
echo 'Delegando permisos'
echo "Error en permisos del sistema......FAIL".	" [ ".$_hoy." ]" >> testing.server.log 
chmodbin
else
echo 'Permisos de ejecución estan correctos'
echo "Permisos de ejecución................OK".	" [ ".$_hoy." ]" >> testing.server.log 
fi
}

chmodbin(){
chmod +x $bin/servicio.sh
}
servicios() {
servicio=$bin/servicio.sh
if [ ! -e "$servicio" ];
then
    echo "File does not exist $servicio"
    echo "Download sh deployer to system_".		"[".$_hoy."]"
    cp servicio.sh $bin/
	
    echo 'Downloading finish OK'
    echo 'Verificando Permisos'
    echo "Downloadings data servicio.sh to working deploy ".	" [ ".$_hoy." ]" >> testing.server.log 
    dos2unix $bin/servicio.sh
    permisosverificarserver
else
    echo "Data Deploy is existed".	" [ ".$_hoy." ]"
    echo "Data deploy is existed  OK ".	" [ "$_hoy" ]" >> testing.server.log
    echo "Directorio de ejecución OK ".	" [ "$_pwd" ]" >> testing.server.log
    echo "Directorio de ejecución OK ".	" [ "$_pwd" ]"
fi
}


# You can install:
#	Translations for CodeIgniter System Messages (translations)
#	CodeIgniter Rest Server (restserver)
#	Codeigniter Matches CLI (matches-cli)
#	CodeIgniter HMVC Modules (jenssegers) (hmvc-modules)
#	Modular Extensions - HMVC (wiredesignz) (modular-extensions-hmvc)
#	Codeigniter Ion Auth (ion-auth)
#	CodeIgniter3 Filename Checker (filename-checker)
#	CodeIgniter Developer Toolbar (codeigniter-develbar)

# Usage:
# php install.php <package> <version/branch>

# Examples:
  php $bin/install.php translations 3.1.0 > instalado.log
  php $bin/install.php restserver 2.7.2 >> instalado.log
  php $bin/install.php matches-cli master >> instalado.log
  php $bin/install.php hmvc-modules master >> instalado.log
  php $bin/install.php modular-extensions-hmvc codeigniter-3.x >> instalado.log
  php $bin/install.php ion-auth 2 >> instalado.log
  php $bin/install.php filename-checker master >> instalado.log
  php $bin/install.php codeigniter-develbar master >> instalado.log
cp install.php $bin/install.php
 # php $bin/install.php sqlite_igniter master >> instalado.log
  find ./ -name "config.php" -print | xargs sed -i "s/index.php//g"
cp servicio.sh $bin/
initiar  
