#
##
## Comando que sirve para renombrar algunos aspectos del sistema
## Esto es utilozado para realizar diferentes arreglos
## La idea fundamentada en usar bases de datos sqlite
## Fundamentalmente un arreglo utilizar en archivos generados.
##
#

#Paso uno
find ./ -name "*.jpg" -print > listado.csv
#Paso dos
sed '/./=' listado.csv | sed '/./N; s/\n/","/;s//./;s/g,/g/'|awk '{print "\x22" $1 , $2"\x22;"}'


sed '/./=' listado.csv | sed '/./N; s/\n/","/;s/g,/g/'|awk '{print
"\x22" $1 , $2"\x22;"}'

echo '1=>Create database'
echo 'dump.db.sqlite'
echo '2=>Set the mode & tablename'

sqlite3 dump.db.sqlite  ".mode csv tripdata"
echo '3=>Import the csv file data to sqlite3'

sqlite3 dump.db.sqlite  ".import listado.ok.csv tripdata"
echo '4=>Find tables'

sqlite3 dump.db.sqlite  ".tables"
echo '5=>Find your table schema'

sqlite3 dump.db.sqlite  ".schema tripdata"
echo '6=>Find table data'

sqlite3 dump.db.sqlite  "select * from tripdata limit 10;"
echo '7=>Count your table data'

sqlite3 dump.db.sqlite  "select count (*) from tripdata;"
