#!/usr/bin/env php
<?php

$installer = new Installer();

if ($argc === 3) {
    $package = $argv[1];
    $version = $argv[2];
    echo $installer->install($package, $version);
} else {
    echo $installer->usage($argv[0]);
}


class Installer
{
    protected $tmp_dir;
    protected $packages = array();

    public function __construct() {
        $this->tmp_dir = __DIR__ . '/tmp';
        @mkdir($this->tmp_dir);
        
        $this->packages = array(
            'translations' => array(
                'site'  => 'github',
                'user'  => 'bcit-ci',
                'repos' => 'codeigniter3-translations',
                'name'  => 'Translations for CodeIgniter System Messages',
                'dir'   => 'language',
                'example_branch' => '3.1.0',
             ),
            'restserver' => array(
                'site'  => 'github',
                'user'  => 'chriskacerguis',
                'repos' => 'codeigniter-restserver',
                'name'  => 'CodeIgniter Rest Server',
                'dir'   => array('config', 'controllers', 'language', 'libraries', 'views'),
                'pre'   => 'application/',
                'msg'   => 'See https://github.com/chriskacerguis/codeigniter-restserver',
                'example_branch' => '2.7.2',
            ),
            'matches-cli' => array(
                'site'  => 'github',
                'user'  => 'avenirer',
                'repos' => 'codeigniter-matches-cli',
                'name'  => 'Codeigniter Matches CLI',
                'dir'   => array('config', 'controllers', 'views'),
                'msg'   => 'See http://avenirer.github.io/codeigniter-matches-cli/',
                'example_branch' => 'master',
            ),
            'hmvc-modules' => array(
                'site'  => 'github',
                'user'  => 'jenssegers',
                'repos' => 'codeigniter-hmvc-modules',
                'name'  => 'CodeIgniter HMVC Modules (jenssegers)',
                'dir'   => array('core', 'third_party'),
                'msg'   => 'See https://github.com/jenssegers/codeigniter-hmvc-modules#installation',
                'example_branch' => 'master',
            ),
            'modular-extensions-hmvc' => array(
                'site'  => 'bitbucket',
                'user'  => 'wiredesignz',
                'repos' => 'codeigniter-modular-extensions-hmvc',
                'name'  => 'Modular Extensions - HMVC (wiredesignz)',
                'dir'   => array('core', 'third_party'),
                'msg'   => 'See https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc',
                'example_branch' => 'codeigniter-3.x',
            ),
            'ion-auth' => array(
                'site'  => 'github',
                'user'  => 'benedmunds',
                'repos' => 'CodeIgniter-Ion-Auth',
                'name'  => 'Codeigniter Ion Auth',
                'dir'   => array(
                    'config', 'controllers', 'language', 'libraries',
                    'migrations', 'models', 'sql', 'views'
                ),
                'msg'   => 'See http://benedmunds.com/ion_auth/',
                'example_branch' => '2',
            ),
            'filename-checker' => array(
                'site'  => 'github',
                'user'  => 'kenjis',
                'repos' => 'codeigniter3-filename-checker',
                'name'  => 'CodeIgniter3 Filename Checker',
                'dir'   => 'controllers',
                'msg'   => 'See https://github.com/kenjis/codeigniter3-filename-checker',
                'example_branch' => 'master',
            ),
            'codeigniter-develbar' => array(
                'site'  => 'github',
                'user'  => 'jcsama',
                'repos' => 'CodeIgniter-develbar',
                'name'  => 'CodeIgniter Developer Toolbar',
                'dir'   => array('config','core', 'third_party','controllers'),
                'msg'   => 'See https://github.com/JCSama/CodeIgniter-develbar',
                'example_branch' => 'master',
            ),

			'sqlite_igniter' => array(
                'site'  => 'github',
                'user'  => 'juanma386',
                'repos' => 'sqlite_igniter',
                'name'  => 'CodeIgniter SQLite Compatibility',
                'dir'   => array('config', 'database'),
				'pre'   => 'application/',
                'msg'   => 'See https://github.com/juanma386/sqlite_igniter',
                'example_branch' => 'master',
            ),
            'MercadoPago_SDK' => array(
                'site'  => 'github',
                'user'  => 'jonymusky',
                'repos' => 'sdk-codeigniter-mercadopago',
                'name'  => 'MercadoPago SDK module for CodeIgniter',
                'dir'   => array('config', 'libraries'),
				'pre'   => 'application/',
                'msg'   => 'See https://github.com/jonymusky/sdk-codeigniter-mercadopago',
                'example_branch' => 'master',
            ),
            'CodeIgniter_Template_Library_basic' => array(
                'site'  => 'github',
                'user'  => 'jenssegers',
                'repos' => 'codeigniter-template-library',
                'name'  => 'CodeIgniter Template Library',
                'dir'   => array('config', 'libraries'),
                'msg'   => 'See basic from https://github.com/jenssegers/codeigniter-template-library',
                'example_branch' => 'master',
            ),
                'CodeIgniter_Template_Library_basic' => array(
                    'site'  => 'github',
                    'user'  => 'kenjis',
                    'repos' => 'codeigniter-twig-samples',
                    'name'  => 'codeigniter twig samples',
                    'dir'   => array('libraries', 'views'),
                    'pre'   => 'application/',
                    'msg'   => 'See basic from https://github.com/kenjis/codeigniter-twig-samples',
                    'example_branch' => 'master',
            ),
            // debugger
            'codeigniter-debugbar' => array(
                'site'  => 'github',
                'user'  => 'kenjis',
                'repos' => 'codeigniter-debugbar',
                'name'  => 'codeigniter debugbar',
                'dir'   => array('libraries','Resources', 'hooks','config'),
                'msg'   => 'See basic from https://github.com/kenjis/codeigniter-debugbar',
                'example_branch' => 'master',
            ),
			
			// mensajes privados
            'codeigniter-mp' => array(
                'site'  => 'github',
                'user'  => 'balint42',
                'repos' => 'ci_pm',
                'name'  => 'codeigniter simple mensaje privados',
                'dir'   => array('controllers','language', 'libraries','models', 'views','config'),
                'msg'   => 'See basic from https://github.com/balint42/ci_pm',
                'example_branch' => 'master',
            ),
			// An easier way to use PHPUnit with CodeIgniter 3.x. 
            'ci-phpunit-test' => array(
                'site'  => 'github',
                'user'  => 'kenjis',
                'repos' => 'ci-phpunit-test',
                'name'  => 'codeigniter simple mensaje privados',
                'dir'   => array('libraries','test','database'),
                'msg'   => 'An easier way to use PHPUnit with CodeIgniter 3.x. from https://github.com/kenjis/ci-phpunit-test',
                'example_branch' => 'master',
            ),
				// An easier way to use IMAP with CodeIgniter 3.x. 
            'ci-imap' => array(
                'site'  => 'github',
                'user'  => 'natanfelles',
                'repos' => 'codeigniter-imap',
                'name'  => 'codeigniter simple mensaje privados',
                'dir'   => array('libraries', 'config'),
                'msg'   => 'An easier way to use IMAP with CodeIgniter 3.x. from https://github.com/natanfelles/codeigniter-imap.git',
                'example_branch' => 'master',
            ),
        );
    }
/*
*
*
|// Custom Checkout
|$config['app_id'] = 'mp-app-14781809'; // not used by the Library
|$config['public_key'] = 'TEST-92287d7d-29e7-4115-bd87-5eda5d9412c6';  // not used by the Library
|$config['access_token'] = 'TEST-7378003262458579-112123-7a96efd0fe98301912c31608739f1ce1-14781809';
|$config['use_access_token'] = TRUE; // TRUE or FALSE
|
|// Basic Checkout
|$config['client_id'] = '7378003262458579';
|$config['client_secret'] = 'TDplRwoLJrwheRzIkqI2xfxUNfGfNt9S';
|
*
*/

 public function usage($self)
    {
        $msg = 'You can install:' . PHP_EOL;
        
        foreach ($this->packages as $key => $value) {
            $msg .= '  ' . $value['name'] . ' (' . $key . ')' . PHP_EOL;
        }
        
        $msg .= PHP_EOL;
        $msg .= 'Usage:' . PHP_EOL;
        $msg .= '  php install.php <package> <version/branch>'  . PHP_EOL;
        $msg .= PHP_EOL;
        $msg .= 'Examples:' . PHP_EOL;

        foreach ($this->packages as $key => $value) {
            $msg .= "  php $self $key " . $value['example_branch'] . PHP_EOL;
        }

        return $msg;
    }

    public function install($package, $version)
    {
        if (! isset($this->packages[$package]))
        {
            return 'Error! no such package: ' . $package . PHP_EOL;
        }

        // github
        if ($this->packages[$package]['site'] === 'github') {
            $method = 'downloadFromGithub';
        } elseif ($this->packages[$package]['site'] === 'bitbucket') {
            $method = 'downloadFromBitbucket';
        } else {
            throw new LogicException(
                'Error! no such repos type: ' . $this->packages[$package]['site']
            );
        }
        
        list($src, $dst) = $this->$method($package, $version);

        $this->recursiveCopy($src, $dst);
        $this->recursiveUnlink($this->tmp_dir);

        $msg = 'Installed: ' . $package .PHP_EOL;
        if (isset($this->packages[$package]['msg'])) {
            $msg .= $this->packages[$package]['msg'] . PHP_EOL;
        }
        return $msg;
    }

    private function downloadFromGithub($package, $version)
    {
        $user = $this->packages[$package]['user'];
        $repos = $this->packages[$package]['repos'];
        $url = "https://github.com/$user/$repos/archive/$version.zip";
        $filepath = $this->download($url);
        $this->unzip($filepath);

        $dir = $this->packages[$package]['dir'];
        $pre = isset($this->packages[$package]['pre']) ? $this->packages[$package]['pre'] : '';
        
        if (is_string($dir)) {
            $src = realpath(dirname($filepath) . "/$repos-$version/$pre$dir");
            $dst = realpath(__DIR__ . "/../application/$dir");
            return array($src, $dst);
        }
        
        foreach ($dir as $directory) {
            $src[] = realpath(dirname($filepath) . "/$repos-$version/$pre$directory");
            @mkdir(__DIR__ . "/../application/$directory");
            $dst[] = realpath(__DIR__ . "/../application/$directory");
        }
        return array($src, $dst);
    }

    private function downloadFromBitbucket($package, $version)
    {
        $user = $this->packages[$package]['user'];
        $repos = $this->packages[$package]['repos'];
        // https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/get/codeigniter-3.x.zip
        $url = "https://bitbucket.org/$user/$repos/get/$version.zip";
        $filepath = $this->download($url);
        $dirname = $this->unzip($filepath);

        $dir = $this->packages[$package]['dir'];
        
        if (is_string($dir)) {
            $src = realpath(dirname($filepath) . "/$dirname/$dir");
            $dst = realpath(__DIR__ . "/../application/$dir");
            return array($src, $dst);
        }
        
        foreach ($dir as $directory) {
            $src[] = realpath(dirname($filepath) . "/$dirname/$directory");
            @mkdir(__DIR__ . "/../application/$directory");
            $dst[] = realpath(__DIR__ . "/../application/$directory");
        }
        return array($src, $dst);
    }

    private function download($url)
    {
        $file = file_get_contents($url);
        if ($file === false) {
            throw new RuntimeException("Can't download: $url");
        }
        echo 'Downloaded: ' . $url . PHP_EOL;
        
        $urls = parse_url($url);
        $filepath = $this->tmp_dir . '/' . basename($urls['path']);
        file_put_contents($filepath, $file);
        
        return $filepath;
    }

    private function unzip($filepath)
    {
        $zip = new ZipArchive();
        if ($zip->open($filepath) === TRUE) {
            $tmp = explode('/', $zip->getNameIndex(0));
            $dirname = $tmp[0];
            $zip->extractTo($this->tmp_dir . '/');
            $zip->close();
        } else {
            throw new RuntimeException('Failed to unzip: ' . $filepath);
        }
        
        return $dirname;
    }

    /**
     * Recursive Copy
     *
     * @param string $src
     * @param string $dst
     */
    private function recursiveCopy($src, $dst)
    {
        if ($src === false) {
            return;
        }

        if (is_array($src)) {
            foreach ($src as $key => $source) {
                $this->recursiveCopy($source, $dst[$key]);
            }
            
            return;
        }

        @mkdir($dst, 0755);
        
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($src, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                @mkdir($dst . '/' . $iterator->getSubPathName());
            } else {
                $success = copy($file, $dst . '/' . $iterator->getSubPathName());
                if ($success) {
                    echo 'copied: ' . $dst . '/' . $iterator->getSubPathName() . PHP_EOL;
                }
            }
        }
    }

    /**
     * Recursive Unlink
     *
     * @param string $dir
     */
    private function recursiveUnlink($dir)
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::CHILD_FIRST
        );
        
        foreach ($iterator as $file) {
            if ($file->isDir()) {
                rmdir($file);
            } else {
                unlink($file);
            }
        }
        
        rmdir($dir);
    }
}
